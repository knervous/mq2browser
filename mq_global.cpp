#include "mq_global.h"
#include <string>
#include <regex>

namespace jsmq {
	enum string_code {
		log,
		warn,
		run,
		eval,
		getSpawnById,
		getZoneSpawns,

	};
	jsmq::string_code hashit(std::string const& inString) {
		if (inString == "log") return jsmq::log;
		if (inString == "warn") return jsmq::warn;
		if (inString == "run") return jsmq::run;
		if (inString == "eval") return jsmq::eval;
		if (inString == "getSpawnById") return jsmq::getSpawnById;
		if (inString == "getZoneSpawns") return jsmq::getZoneSpawns;

	}

}

CefRefPtr<CefV8Value> MapSpawn(PSPAWNINFO spawn);


bool MqFunctionMapper::Execute(const CefString& name,
	CefRefPtr<CefV8Value> object,
	const CefV8ValueList& arguments,
	CefRefPtr<CefV8Value>& retval,
	CefString& exception) {

	switch (jsmq::hashit(name)) {
	case jsmq::log:
		for (auto str : arguments) {
			if (str->IsString()) {
				WriteChatColor(str->GetStringValue().ToString().c_str());
			}
		}
		break;
	case jsmq::warn:
		for (auto str : arguments) {
			if (str->IsString()) {
				WriteChatColor(str->GetStringValue().ToString().c_str(), CONCOLOR_YELLOW);
			}
		}
		break;
	case jsmq::run:
		for (auto str : arguments) {
			if (str->IsString()) {
				std::string command_str("/");
				command_str += str->GetStringValue().ToString().c_str();
				try {
					DoCommand(GetCharInfo()->pSpawn, (PCHAR)command_str.c_str());
				}
				catch (...) {
					return false;
				}
			}
		}
		break;
	case jsmq::eval:
		if (!arguments.empty() && arguments[0]->IsString()) {
			std::string str("${");
			str += arguments[0]->GetStringValue().ToString();
			str += "}";
			auto eval_string = std::string(ParseMacroParameter(GetCharInfo()->pSpawn, (PCHAR)str.c_str(), MAX_STRING));
			// Try to coerce values through double, bool, then fallback to string
			double num;
			std::regex double_regex("^(-?)(0|([1-9][0-9]*))(\.[0-9]+)?$");
			if (std::regex_match(eval_string, double_regex) && parse_double(eval_string, num)) {
				retval = CefV8Value::CreateDouble(num);
				break;
			}
			retval = eval_string == "TRUE" ?
				CefV8Value::CreateBool(true) :
				eval_string == "FALSE" ?
				CefV8Value::CreateBool(false) :
				eval_string == "NULL" ? CefV8Value::CreateNull() :
				CefV8Value::CreateString(eval_string);
		}
		break;
	case jsmq::getSpawnById:
		if (!arguments.empty() && arguments[0]->IsInt()) {
			retval = MapSpawn(GetSpawnByID(arguments[0]->GetIntValue()));
		}
		else {
			WriteChatColor("Bad args for getNthNearestSpawn, arg 1 is string and opt arg 2 is int", CONCOLOR_YELLOW);
		}
		break;

	case jsmq::getZoneSpawns:
	{
		PSPAWNINFO pAllSpawns = (PSPAWNINFO)pSpawnList;
		CefRefPtr<CefV8Value> zone_spawns = CefV8Value::CreateArray(0);
		if (pSpawnManager)
		{
			SPAWNINFO* pSpawn = pSpawnList;
			while (pSpawn)
			{
				zone_spawns->SetValue(zone_spawns->GetArrayLength(), MapSpawn(pSpawn));
				pSpawn = pSpawn->pNext;
			}
		}
		retval = zone_spawns;
		break;
	}

	default:
		return false;
	}
	return true;
}

// Convenience function to not have to pull this out in runtime and to be able to map arrays here in core code
CefRefPtr<CefV8Value> MapSpawn(PSPAWNINFO spawn) {
	if (spawn == NULL) {
		return CefV8Value::CreateNull();
	}
	auto obj = CefV8Value::CreateObject(nullptr, nullptr);
	obj->SetValue("name", CefV8Value::CreateString(spawn->Name), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("displayedName", CefV8Value::CreateString(spawn->DisplayedName), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("level", CefV8Value::CreateInt(spawn->Level), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("x", CefV8Value::CreateDouble(spawn->X), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("y", CefV8Value::CreateDouble(spawn->Y), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("z", CefV8Value::CreateDouble(spawn->Z), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("type", CefV8Value::CreateInt(spawn->Type), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("id", CefV8Value::CreateInt(spawn->SpawnID), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("hp", CefV8Value::CreateDouble(spawn->HPCurrent), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("maxHp", CefV8Value::CreateDouble(spawn->HPMax), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("mana", CefV8Value::CreateDouble(spawn->ManaCurrent), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("maxMana", CefV8Value::CreateDouble(spawn->ManaMax), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("heading", CefV8Value::CreateDouble(spawn->Heading), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("animation", CefV8Value::CreateInt(spawn->Animation), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("showHelm", CefV8Value::CreateBool(spawn->bShowHelm), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("race", CefV8Value::CreateString(pEverQuest->GetRaceDesc(spawn->mActorClient.Race)), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("raceId", CefV8Value::CreateInt(spawn->mActorClient.Race), V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("class", CefV8Value::CreateString(pEverQuest->GetClassDesc(spawn->mActorClient.Class)), V8_PROPERTY_ATTRIBUTE_READONLY);
	return obj;
}

bool MqGlobal::Get(const CefString& name,
	const CefRefPtr<CefV8Value> object,
	CefRefPtr<CefV8Value>& retval,
	CefString& exception) {
	retval = CefV8Value::CreateNull();
	switch (jsmq::hashit(name)) {
	default:
		retval = CefV8Value::CreateFunction(name, new MqFunctionMapper);
		break;
	}
	return !retval->IsNull();
}


void MapGlobal(CefRefPtr<CefV8Value> global) {
	std::vector<std::string> property_list{
		"log",
		"warn",
		"run",
		"eval",
		"getSpawnById",
		"getZoneSpawns",

	};

	CefRefPtr<CefV8Accessor> accessor = new MqGlobal();
	CefRefPtr<CefV8Value> obj = CefV8Value::CreateObject(accessor, nullptr);

	std::function<void(const char*)> set_accessor =
		[&obj](
			const char* accessor) -> void {
		obj->SetValue(accessor, V8_ACCESS_CONTROL_PROHIBITS_OVERWRITING, V8_PROPERTY_ATTRIBUTE_READONLY);
	};

	for (std::string attribute : property_list) {
		set_accessor(attribute.c_str());
	}

	global->SetValue("mq", obj, V8_PROPERTY_ATTRIBUTE_READONLY);
};
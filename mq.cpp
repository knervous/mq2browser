#include "mq.h"


void MapGlobal(CefRefPtr<CefV8Value> global) {
	CefRefPtr<CefV8Accessor> accessor = new Mq();
	CefRefPtr<CefV8Value> obj = CefV8Value::CreateObject(accessor, nullptr);
	obj->SetValue("myProp", V8_ACCESS_CONTROL_PROHIBITS_OVERWRITING, V8_PROPERTY_ATTRIBUTE_READONLY);
	obj->SetValue("myFunc", V8_ACCESS_CONTROL_PROHIBITS_OVERWRITING, V8_PROPERTY_ATTRIBUTE_READONLY);

	global->SetValue("mq", obj, V8_PROPERTY_ATTRIBUTE_READONLY);
}
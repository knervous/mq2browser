#pragma once
#include <cef_v8.h>
#include <string.h>
#include "include/cef_base.h"
#include "include/wrapper/cef_helpers.h"
#include "util.h"
#include "MQ2Plugin.h"
#include <mq/Plugin.h>

#include <mq/plugin/pluginapi.h>

// All Functions
class MqFunctionMapper : public CefV8Handler {
	virtual bool Execute(const CefString& name,
		CefRefPtr<CefV8Value> object,
		const CefV8ValueList& arguments,
		CefRefPtr<CefV8Value>& retval,
		CefString& exception) override;

	// Provide the reference counting implementation for this class.
	IMPLEMENT_REFCOUNTING(MqFunctionMapper);
};

class MqGlobal : public CefV8Accessor {
	virtual bool Get(const CefString& name,
		const CefRefPtr<CefV8Value> object,
		CefRefPtr<CefV8Value>& retval,
		CefString& exception) override;

	virtual bool Set(const CefString& name,
		const CefRefPtr<CefV8Value> object,
		const CefRefPtr<CefV8Value> value,
		CefString& exception) override {
		return false;
	}

	// Provide the reference counting implementation for this class.
	IMPLEMENT_REFCOUNTING(MqGlobal);
};


void MapGlobal(CefRefPtr<CefV8Value> global);
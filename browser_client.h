// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.


#pragma once
#include <iostream>
#include <mutex>
#include <thread>
#include "d3dx9.h"

//#include "imgui_impl_dx9.h"
#include "imgui_internal.h"
#include "include/cef_app.h"
#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"
#include "include/wrapper/cef_helpers.h"

// for manual render handler
class BrowserClient : public CefClient,
	public CefLifeSpanHandler,
	public CefLoadHandler,
	public CefRenderHandler,
	private CefDisplayHandler,
	private CefContextMenuHandler,
	private CefJSDialogHandler,
	// private CefFocusHandler,
	private CefDialogHandler,
	private CefRequestHandler,
	private CefResourceRequestHandler {
public:
	BrowserClient(int w, int h, LPDIRECT3DDEVICE9 device);

	~BrowserClient() {}

	static BrowserClient* GetInstance();

	virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() { return this; }
	virtual CefRefPtr<CefLoadHandler> GetLoadHandler() { return this; }
	virtual CefRefPtr<CefRenderHandler> GetRenderHandler() { return this; }
	virtual CefRefPtr<CefDisplayHandler> GetDisplayHandler() { return this; }

	virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser);
	virtual bool DoClose(CefRefPtr<CefBrowser> browser);
	virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser);
	virtual bool OnBeforePopup(
		CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		const CefString& target_url,
		const CefString& target_frame_name,
		CefLifeSpanHandler::WindowOpenDisposition target_disposition,
		bool user_gesture,
		const CefPopupFeatures& popupFeatures,
		CefWindowInfo& windowInfo,
		CefRefPtr<CefClient>& client,
		CefBrowserSettings& settings,
		CefRefPtr<CefDictionaryValue>& extra_info,
		bool* no_javascript_access) override;

	virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		int httpStatusCode);

	virtual bool OnLoadError(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		CefLoadHandler::ErrorCode errorCode,
		const CefString& failedUrl,
		CefString& errorText);

	virtual void OnLoadingStateChange(CefRefPtr<CefBrowser> browser,
		bool isLoading,
		bool canGoBack,
		bool canGoForward);

	virtual void OnLoadStart(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame);

	// CefRenderHandler methods
	virtual void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) override;
	virtual void OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect) override;
	virtual void GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
	virtual void OnPaint(CefRefPtr<CefBrowser> browser,
		PaintElementType type,
		const RectList& dirtyRects,
		const void* buffer,
		int w,
		int h) override;

	// Our functions
	void Draw(bool* show);
	void Shutdown();
	void Pause();
	void Resume();
	CefRefPtr<CefBrowser> GetBrowser();
private:
	void SetZoomLevel();
	void SetAudioVolume();
	char url_buffer[2048];
	std::string page_title;
	CefRefPtr<CefBrowser> web_browser;
	LPDIRECT3DTEXTURE9 pTexture = NULL;
	int browser_id;
	float opacity = 1.0f;
	float volume = 1.0f;
	float zoom = 0.0f;
	bool input_has_focus = false;
	bool browser_has_focus = false;
	bool browser_hovered = false;
	bool background = true;
	bool dev_tools_open = false;
	bool closing = false;
	bool loaded = false;
	bool show_config = false;
	bool can_back = false;
	bool can_forward = false;
	CefRefPtr<CefRenderHandler> handler;
	int width;
	std::string url;
	int height;
	LPDIRECT3DDEVICE9 device;
	bool m_mouseButtonStates[3];
	struct {
		bool changed = false;
		std::mutex dataMutex;
		std::mutex cvMutex;
		std::condition_variable cv;

		const void* buffer;
		int width, height;
		CefRenderHandler::RectList dirtyRects;

		CefRect popupRect;
		bool popupShown = false;
		std::unique_ptr<byte[]> popupBuffer;
	} render_data;
	IMPLEMENT_REFCOUNTING(BrowserClient);
};
// namespace

#include "mq_web_app.h"
#include <include/wrapper/cef_stream_resource_handler.h>
#include <include/cef_parser.h>

#define NDEBUG
#undef OFFICIAL_BUILD

#ifdef MQ_BROWSER
#include "mq_global.h"
#include "util.h"
#endif

void MqWebApp::OnRegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> registrar) {
	registrar->AddCustomScheme("mq", CEF_SCHEME_OPTION_CSP_BYPASSING);
};
void MqWebApp::OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line) {
	// command_line->AppendSwitch("disable-gpu-compositing");
	// command_line->AppendSwitch("disable-gpu");
	// command_line->AppendSwitch("disable-d3d11");
	command_line->AppendSwitch("enable-begin-frame-scheduling");
	command_line->AppendSwitch("single-process");

	if (process_type.empty())
	{
		command_line->AppendSwitchWithValue("autoplay-policy", "no-user-gesture-required");
		command_line->AppendSwitchWithValue("enable-blink-features", "ShadowDOMV0,CustomElementsV0,HTMLImports");
	}
};

// CefSchemeHandlerFactory methods
CefRefPtr<CefResourceHandler> MqWebApp::Create(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& scheme_name,
	CefRefPtr<CefRequest> request) {
	return nullptr;
};


void MqWebApp::OnContextCreated(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefRefPtr<CefV8Context> context) {
	if (shutdown_) {
		return;
	}
	CEF_REQUIRE_RENDERER_THREAD();
	auto global = context->GetGlobal();
	CefRefPtr<CefV8Handler> handler = new EventCallbackFunctionMapper(&callback_map_);

	// Global listener function for callbacks like spawn events
	CefRefPtr<CefV8Value> func = CefV8Value::CreateFunction("addMqListener", handler);

	// Add the "addMqListener" function to the "window" object.
	global->SetValue("addMqListener", func, V8_PROPERTY_ATTRIBUTE_NONE);
#ifdef MQ_BROWSER
	MapGlobal(global);
#endif
}

void MqWebApp::OnContextInitialized() {
	if (shutdown_) {
		return;
	}
	CEF_REQUIRE_UI_THREAD();
	CefRefPtr<CefCommandLine> command_line =
		CefCommandLine::GetGlobalCommandLine();

	CefBrowserSettings browser_settings;

	CefWindowInfo window_info;
	CefBrowserSettings browserSettings;

	// Use off screen rendering so we can draw right to our dx9 surface
	window_info.SetAsWindowless(NULL);

	CefRefPtr<BrowserClient> browserClient(
		new BrowserClient(400, 400, device_));

	browser_ = CefBrowserHost::CreateBrowserSync(window_info, browserClient.get(),
		"https://google.com", browserSettings,
		nullptr, nullptr);
	init_ = true;
	shutdown_ = false;
};

void MqWebApp::OnContextReleased(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefRefPtr<CefV8Context> context) {
	// Remove any JavaScript callbacks registered for the context that has been released.
	if (!callback_map_.empty()) {
		CallbackMap::iterator it = callback_map_.begin();
		for (; it != callback_map_.end();) {
			if (it->second.first->IsSame(context))
				callback_map_.erase(it++);
			else
				++it;
		}
	}
}
CefRefPtr<CefClient> MqWebApp::GetDefaultClient() {
	return BrowserClient::GetInstance();
};

bool MqWebApp::OnProcessMessageReceived(
	CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefProcessId source_process,
	CefRefPtr<CefProcessMessage> message) {
	bool handled = false;
	if (!callback_map_.empty()) {
		const CefString& message_name = message->GetName();
		auto name = message_name.ToString();
		auto browser_id = browser->GetIdentifier();
		CallbackMap::const_iterator it = callback_map_.find(
			std::make_pair(name, browser_id));
		if (it != callback_map_.end()) {
			CefRefPtr<CefV8Context> context = it->second.first;
			CefRefPtr<CefV8Value> callback = it->second.second;
			context->Enter();

			CefV8ValueList arguments;

			// First argument is the message name.
			arguments.push_back(CefV8Value::CreateString(name));

			// Second argument is the list of message arguments.
			CefRefPtr<CefListValue> list = message->GetArgumentList();
#ifdef MQ_BROWSER
			if (name == "spawn") {
				arguments.push_back(CefV8Value::CreateInt(list->GetInt(0)));
			}
			else if (name == "chat" || name == "despawn") {
				arguments.push_back(CefV8Value::CreateString(list->GetString(0)));
			}
#endif
			CefRefPtr<CefV8Value> retval = callback->ExecuteFunction(nullptr, arguments);
			if (retval.get()) {
				if (retval->IsBool())
					handled = retval->GetBoolValue();
			}

			context->Exit();
		}
	}
	return handled;
};

void MqWebApp::LoadUrl(std::string url) {
	browser_->GetMainFrame()->LoadURL(url);
}

#ifdef MQ_BROWSER
bool MqWebApp::OnIncomingChat(const char* line, DWORD Color) {
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("chat");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();
	args->SetString(0, line);

	if (browser_ != nullptr && browser_->GetMainFrame() != nullptr) {
		browser_->GetMainFrame()->SendProcessMessage(PID_RENDERER, msg);
	}
	return false;
};
void MqWebApp::OnAddSpawn(PSPAWNINFO pNewSpawn) {
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("spawn");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();

	// Spawn ID to instantiate in render thread
	args->SetInt(0, pNewSpawn->GetId());

	if (browser_ != nullptr && browser_->GetMainFrame() != nullptr) {
		browser_->GetMainFrame()->SendProcessMessage(PID_RENDERER, msg);
	}
};
void MqWebApp::OnRemoveSpawn(PSPAWNINFO pSpawn) {
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("despawn");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();

	// Spawn ID to instantiate in render thread
	args->SetString(0, pSpawn->DisplayedName);

	if (browser_ != nullptr && browser_->GetMainFrame() != nullptr) {
		browser_->GetMainFrame()->SendProcessMessage(PID_RENDERER, msg);
	}
};
void MqWebApp::OnBeginZone() {
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("beginZone");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();

	if (browser_ != nullptr && browser_->GetMainFrame() != nullptr) {
		// Suspend rendering while zoning
		browser_->GetHost()->WasHidden(true);
		browser_->GetMainFrame()->SendProcessMessage(PID_RENDERER, msg);
	}
}
void MqWebApp::OnEndZone()
{
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("endZone");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();

	if (browser_ != nullptr && browser_->GetMainFrame() != nullptr) {
		// Resume rendering when zoning is finished
		if (!shutdown_) {
			browser_->GetHost()->WasHidden(false);
		}
		browser_->GetMainFrame()->SendProcessMessage(PID_RENDERER, msg);
	}
}
void MqWebApp::OnZoned() {
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("zone");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();

	if (browser_ != nullptr && browser_->GetMainFrame() != nullptr) {
		browser_->GetMainFrame()->SendProcessMessage(PID_RENDERER, msg);
	}
}
#endif

void MqWebApp::Draw(bool* show) {
	if (!init_ || shutdown_) {
		return;
	}

	BrowserClient::GetInstance()->Draw(show);
};

void MqWebApp::Shutdown() {
	browser_->GetHost()->WasHidden(true);
	// Just pause for now. We can't shut down CEF and hope to re-init
	BrowserClient::GetInstance()->Pause();
	shutdown_ = true;
};

void MqWebApp::Resume() {
	BrowserClient::GetInstance()->Resume();
	browser_->GetHost()->WasHidden(false);
	shutdown_ = false;
}

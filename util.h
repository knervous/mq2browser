#pragma once
#include <cef_v8.h>
#include <string.h>

bool is_number(const std::string& s);

bool parse_double(std::string in, double& res);
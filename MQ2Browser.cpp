// MQ2Browser.cpp : Defines the entry point for the DLL application.
//

// PLUGIN_API is only to be used for callbacks.  All existing callbacks at this time
// are shown below. Remove the ones your plugin does not use.  Always use Initialize
// and Shutdown for setup and cleanup.

#include <mq/Plugin.h>
#include "browser_client.h"
#include "mq_web_app.h"
#include "eqlib/WindowOverride.h"

PreSetup("MQ2Browser");
PLUGIN_VERSION(0.1);

static int s_lastWindowIndex = -1;
const int BUTTON_SPACING = 22;

static bool s_refreshItemDisplay = false;

bool ShowMQ2BrowserWindow = false;
bool DidInit = false;

CefRefPtr<MqWebApp> app;

void HandleBrowserButton(const ItemPtr& pItem)
{
	if (pItem && app != nullptr && DidInit)
	{
		ShowMQ2BrowserWindow = true;
		std::string url("https://lucy.allakhazam.com/item.html?id=");
		url += std::to_string(pItem->GetID());
		app->LoadUrl(url);
	}
}

static void check() {};

void BrowserCommand(SPAWNINFO* pChar, char* szLine)
{
	bRunNextCommand = true;

	char szArg[MAX_STRING] = { 0 };
	GetArg(szArg, szLine, 1);
	const char* szRest = GetNextArg(szLine);

	if (!_strnicmp(szArg, "show", 4)) // Display Help
	{
		ShowMQ2BrowserWindow = true;
	}
	else if (!_strnicmp(szArg, "hide", 4)) {
		ShowMQ2BrowserWindow = false;
	}
}

PLUGIN_API void InitializePlugin()
{
	DebugSpewAlways("MQ2Browser::Initializing version %f", MQ2Version);


	AddCommand("/browser", BrowserCommand);
	if (app != nullptr) {
		app->Resume();
	}
}
PLUGIN_API void ShutdownPlugin()
{
	RemoveCommand("/browser");

	// let's debug
	if (DidInit) {

		// Can only initialize CEF once per process
		// Just hide off screen and stop rendering for now
		if (app != nullptr) {
			app->Shutdown();
		}

		// Leak intentionally. Because CEF can only initialize once, we only have a few options:
		// 1. Run CefShutdown which leads to a crash
		// 2. Do nothing and let this module be GC which leads to a crash
		// 3. Keep this module in memory via this hack but suspend the browser to minimize resource use
		char path[MAX_PATH];
		HMODULE hm = NULL;
		if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
			GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
			(LPCSTR)&check, &hm) == 0)
		{
			int ret = GetLastError();
			fprintf(stderr, "GetModuleHandle failed, error = %d\n", ret);
			return;
		}
		GetModuleFileNameA(hm, path, sizeof(path));

		// increase ref count so we never unload
		::LoadLibraryA(path);
	}

	DebugSpewAlways("MQ2Browser::Shutting down");
}

PLUGIN_API void OnCleanUI()
{
}

PLUGIN_API bool OnIncomingChat(const char* Line, DWORD Color)
{
	if (app != nullptr) {
		app->OnIncomingChat(Line, Color);
	}
	return false;
}
PLUGIN_API void OnAddSpawn(PSPAWNINFO pNewSpawn)
{
	if (app != nullptr) {
		app->OnAddSpawn(pNewSpawn);
	}
}
PLUGIN_API void OnRemoveSpawn(PSPAWNINFO pSpawn)
{
	if (app != nullptr) {
		app->OnRemoveSpawn(pSpawn);
	}
}
PLUGIN_API void OnAddGroundItem(PGROUNDITEM pNewGroundItem)
{
	// DebugSpewAlways("MQ2Browser::OnAddGroundItem(%d)", pNewGroundItem->DropID);
}
PLUGIN_API void OnRemoveGroundItem(PGROUNDITEM pGroundItem)
{
	// DebugSpewAlways("MQ2Browser::OnRemoveGroundItem(%d)", pGroundItem->DropID);
}
PLUGIN_API void OnBeginZone()
{
	if (app != nullptr) {
		app->OnBeginZone();
	}
}
PLUGIN_API void OnEndZone()
{
	if (app != nullptr) {
		app->OnEndZone();
	}
}
PLUGIN_API void OnZoned()
{
	if (app != nullptr) {
		app->OnZoned();
	}
}
PLUGIN_API void OnUpdateImGui()
{
	if (ShowMQ2BrowserWindow)
	{
		// Only initialize once on the first time ShowMQ2BrowserWindow is true so we don't load
		// CEF
		if (!DidInit) {
			// Setup CEF
			CefMainArgs args;
			CefSettings settings;
			LPDIRECT3DDEVICE9 device;
			if (pGraphicsEngine && pGraphicsEngine->pRender && pGraphicsEngine->pRender->pD3DDevice)
			{
				device = pGraphicsEngine->pRender->pD3DDevice;
			}
			else {
				WriteChatColor("Could not initialize display");
				return;
			}
			app = new MqWebApp(device);
			char path[256];
#ifdef MQ_BROWSER
			strcpy(path, (std::string(gPathMQRoot) + "/cef_cache").c_str());
#else
			strcpy(path, "c:\\temp\\cef_cache");
#endif

			CefString(&settings.cache_path) = path;
			CefString(&settings.root_cache_path) = path;
			settings.no_sandbox = true;
			settings.pack_loading_disabled = false;
			settings.windowless_rendering_enabled = true;
			settings.multi_threaded_message_loop = true;
			settings.command_line_args_disabled = true;
			settings.persist_user_preferences = true;
			CefMainArgs main_args;
			int exit_code = CefExecuteProcess(main_args, app.get(), nullptr);
			if (exit_code >= 0) {
				// The sub-process terminated, exit now.
				return;
			}

			CefInitialize(args, settings, app.get(), nullptr);
			CefRegisterSchemeHandlerFactory("mq", "", app.get());
			DidInit = true;
		}
		if (app != nullptr) {
			app->Draw(&ShowMQ2BrowserWindow);
		}

	}
}
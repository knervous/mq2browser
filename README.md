# MQ2Browser

This plugin embeds a Chromium browser within the EQ window that can be used more or less like a normal browser. It inherits all the movement and docking of other ImGui windows as well as configuration for opacity and background display.

## Getting Started

Load the plugin

```txt
/plugin MQ2Browser
```

### Commands

```javascript
/browser show // shows the browser
/browser hide // hides the browser
```

## Example

![howto](img/browser1.png)

## MQ2 Dashboard

There is a web application in the works that will interact with MQ core functionality to help visualize and interact with the game through the Menu. This web application will be updated periodically to introduce new features and bug fixes. No downloading of newer MQ versions is necesssary to receive these updates.

A feature within this dashboard that may prove useful is the Advanced Map feature which displays all spawns on the map. There are currently restrictions to which zones will be displayed and updates / workarounds will be available accordingly.

![advancedmap](img/browser2.png)

### Configuration File

Currently no configuration file.

## Other Notes

Due to the framework's limitations of loading and unloading, once the plugin is loaded, it can never be unloaded within the lifetime of that eqgame.exe - you will have to restart to free the memory. The browser will only load the first time you run `/browser show`

## Authors

- knervous

## Bugs / feature requests

- Please contact mqnode@gmail.com or @knervous in MQ discord

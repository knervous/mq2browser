#pragma once
#include <iostream>
#include <mutex>
#include <thread>
#include <list>
#include "d3dx9.h"
#include "browser_client.h"
//#include "imgui_impl_dx9.h"
#include "imgui_internal.h"
#include "include/cef_app.h"
#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"
#include "include/wrapper/cef_helpers.h"

#ifdef MQ_BROWSER
#include <mq/Plugin.h>
#endif


typedef std::map<std::pair<std::string, int>,
	std::pair<CefRefPtr<CefV8Context>, CefRefPtr<CefV8Value> > >
	CallbackMap;

class EventCallbackFunctionMapper : public CefV8Handler {
public:
	EventCallbackFunctionMapper::EventCallbackFunctionMapper(CallbackMap* map) : callback_map_(map) {};
	bool Execute(const CefString& name,
		CefRefPtr<CefV8Value> object,
		const CefV8ValueList& arguments,
		CefRefPtr<CefV8Value>& retval,
		CefString& exception) override {
		if (arguments.size() == 2 && arguments[0]->IsString() &&
			arguments[1]->IsFunction()) {
			std::string message_name = arguments[0]->GetStringValue();
			CefRefPtr<CefV8Context> context = CefV8Context::GetCurrentContext();
			int browser_id = context->GetBrowser()->GetIdentifier();
			callback_map_->insert(
				std::make_pair(std::make_pair(message_name, browser_id),
					std::make_pair(context, arguments[1])));
		}
		return true;
	}

private:
	CallbackMap* callback_map_;
	// Provide the reference counting implementation for this class.
	IMPLEMENT_REFCOUNTING(EventCallbackFunctionMapper);
};


// Implement application-level callbacks for the browser process.
class MqWebApp : public CefApp,
	public CefBrowserProcessHandler,
	public CefRenderProcessHandler,
	public CefSchemeHandlerFactory {
public:
	MqWebApp(LPDIRECT3DDEVICE9 device)
		: device_(device) {};

	virtual void OnRegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> registrar) override;
	virtual void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line) override;

	// CefSchemeHandlerFactory methods
	virtual CefRefPtr<CefResourceHandler> Create(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& scheme_name,
		CefRefPtr<CefRequest> request) override;


	// CefApp methods:
	CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override {
		return this;
	}

	CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override {
		return this;
	}

	void OnContextCreated(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		CefRefPtr<CefV8Context> context);

	// CefBrowserProcessHandler methods:
	void OnContextInitialized() override;

	void OnContextReleased(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		CefRefPtr<CefV8Context> context) override;

	CefRefPtr<CefClient> GetDefaultClient() override;

	bool OnProcessMessageReceived(
		CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		CefProcessId source_process,
		CefRefPtr<CefProcessMessage> message) override;

	void Draw(bool* show);
	void Shutdown();
	void Resume();
	void LoadUrl(std::string url);
#ifdef MQ_BROWSER
	bool OnIncomingChat(const char* Line, DWORD Color);
	void OnAddSpawn(PSPAWNINFO pNewSpawn);
	void OnRemoveSpawn(PSPAWNINFO pSpawn);
	void OnBeginZone();
	void OnEndZone();
	void OnZoned();
#endif
private:
	CefRefPtr<CefBrowser> browser_;
	CallbackMap callback_map_;
	bool init_ = false;
	bool shutdown_ = false;
	LPDIRECT3DDEVICE9 device_;
	// Include the default reference counting implementation.
	IMPLEMENT_REFCOUNTING(MqWebApp);
};

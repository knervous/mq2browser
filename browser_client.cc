// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <algorithm>
#include <regex>
#include "browser_client.h"
#include "imgui.h"
#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"
#include "include/wrapper/cef_helpers.h"
#include "imgui/fonts/IconsMaterialDesign.h"
#ifdef MQ_BROWSER
#include <mq/Plugin.h>
#endif
BrowserClient* g_instance = nullptr;

enum eWebBrowserMouseButton {
	BROWSER_MOUSEBUTTON_LEFT = 0,
	BROWSER_MOUSEBUTTON_MIDDLE = 1,
	BROWSER_MOUSEBUTTON_RIGHT = 2
};



BrowserClient::BrowserClient(int w, int h, LPDIRECT3DDEVICE9 dev)
	: width(w), height(h) {
	g_instance = this;
	browser_id = -1;
	device = dev;
	strcpy(url_buffer, "https://www.google.com/");
	memset(m_mouseButtonStates, 0, sizeof(m_mouseButtonStates));
	auto result = D3DXCreateTexture(dev, w, h, 1, 0, D3DFMT_A8R8G8B8,
		D3DPOOL_MANAGED, &pTexture);
}

// static
BrowserClient* BrowserClient::GetInstance() {
	return g_instance;
}

CefRefPtr<CefBrowser> BrowserClient::GetBrowser() { return web_browser; }

// CefLifeSpanHandler methods.
void BrowserClient::OnAfterCreated(CefRefPtr<CefBrowser> browser) {
	CEF_REQUIRE_UI_THREAD();
	web_browser = browser;
	browser_id = browser->GetIdentifier();
}

bool BrowserClient::DoClose(CefRefPtr<CefBrowser> browser) {
	CEF_REQUIRE_UI_THREAD();
	return false;
}

void BrowserClient::OnBeforeClose(CefRefPtr<CefBrowser> browser) {}

void BrowserClient::OnLoadEnd(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	int httpStatusCode) {
	if (browser->GetMainFrame()->GetURL() == "devtools://devtools/devtools_app.html") {
		return;
	}
	strcpy(url_buffer, browser->GetMainFrame()->GetURL().ToString().c_str());
	printf("OnLoadEnd(%d)\n", httpStatusCode);
	loaded = true;
}

bool BrowserClient::OnLoadError(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefLoadHandler::ErrorCode errorCode,
	const CefString& failedUrl,
	CefString& errorText) {
	printf("OnLoadError(%d)\n", errorCode);
	loaded = true;
	return loaded;
}

void BrowserClient::OnLoadingStateChange(CefRefPtr<CefBrowser> browser,
	bool isLoading,
	bool canGoBack,
	bool canGoForward) {
	can_back = canGoBack;
	can_forward = canGoForward;
	printf("OnLoadingStateChange()\n");
}

void BrowserClient::OnLoadStart(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame) {
	printf("OnLoadStart()\n");
}

void BrowserClient::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) {
	rect = CefRect(0, 0, width < 1 ? 1 : width, height < 1 ? 1 : height);
	rect.x = 0;
	rect.y = 0;
	rect.width = width < 1 ? 1 : width;
	rect.height = height < 1 ? 1 : height;
}

bool BrowserClient::OnBeforePopup(
	CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	const CefString& target_url,
	const CefString& target_frame_name,
	CefLifeSpanHandler::WindowOpenDisposition target_disposition,
	bool user_gesture,
	const CefPopupFeatures& popupFeatures,
	CefWindowInfo& windowInfo,
	CefRefPtr<CefClient>& client,
	CefBrowserSettings& settings,
	CefRefPtr<CefDictionaryValue>& extra_info,
	bool* no_javascript_access) {
	// Block popups generally
	return target_frame_name != "DevTools";
}

void BrowserClient::OnPaint(CefRefPtr<CefBrowser> browser,
	PaintElementType type,
	const RectList& dirtyRects,
	const void* buffer,
	int w,
	int h) {
		{
			if (w != width || h != height || w == 0 || h == 0) {
				return;
			}
			std::lock_guard<std::mutex> lock(render_data.dataMutex);

			// Store render data
			render_data.buffer = buffer;
			render_data.width = w;
			render_data.height = h;
			render_data.dirtyRects = dirtyRects;
			render_data.changed = true;
		}

		// Wait for the main thread to handle drawing the texture
		std::unique_lock<std::mutex> lock(render_data.cvMutex);
		render_data.cv.wait(lock);
}

void BrowserClient::SetAudioVolume()
{
	// NOTE: Keep this function thread-safe
	if (!web_browser || volume < 0.0f || volume > 1.0f)
		return;

	// Since the necessary interfaces of the core audio API were introduced in Win7, we've to fallback to HTML5 audio
	std::stringstream jscode;
	jscode << "function mta_adjustAudioVol(elem, vol) { elem.volume = vol; elem.onvolumechange = function() { if (Math.abs(elem.volume - vol) >= 0.001) elem.volume "
		"= vol; } }"
		"var tags = document.getElementsByTagName('audio'); for (var i = 0; i<tags.length; ++i) { mta_adjustAudioVol(tags[i], " << volume << "); }"
		"tags = document.getElementsByTagName('video'); for (var i = 0; i<tags.length; ++i) { mta_adjustAudioVol(tags[i], " << volume << "); }";

	std::vector<CefString> frameNames;
	web_browser->GetFrameNames(frameNames);

	for (auto& name : frameNames)
	{
		auto frame = web_browser->GetFrame(name);
		frame->ExecuteJavaScript(jscode.str(), "", 0);
	}
}

void BrowserClient::SetZoomLevel()
{
	// NOTE: Keep this function thread-safe
	if (!web_browser)
		return;

	web_browser->GetHost()->SetZoomLevel(zoom);
}


void BrowserClient::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
	std::lock_guard<std::mutex> lock{ render_data.dataMutex };
	render_data.popupShown = show;

	// Free popup buffer memory if hidden
	if (!show)
		render_data.popupBuffer.reset();
}

void BrowserClient::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
	std::lock_guard<std::mutex> lock{ render_data.dataMutex };

	// Update rect
	render_data.popupRect = rect;

	// Resize buffer
	render_data.popupBuffer.reset(new byte[rect.width * rect.height * 4]);
}


void BrowserClient::Draw(bool* show) {

	std::lock_guard<std::mutex> lock(render_data.dataMutex);

	if (!pTexture) {
		return;
	}
	ImGuiWindowFlags window_flags = 0;
	window_flags |= ImGuiWindowFlags_NoScrollbar;
	window_flags |= ImGuiWindowFlags_MenuBar;
	window_flags |= ImGuiWindowFlags_NoNav;
	if (!background) {
		window_flags |= ImGuiWindowFlags_NoBackground;
	}
	if (browser_hovered) {
		window_flags |= ImGuiWindowFlags_NoMove;
	}
	auto io = ImGui::GetIO();
	bool loadNewUrl = false;
	auto skip_validation = false;
	auto input_focused = false;
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
	ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.9, 0.9, 0.4, 0.9));
	ImGui::SetNextWindowSize(ImVec2(600, 800), ImGuiCond_FirstUseEver);
	auto browser_tab_visible = ImGui::Begin("MQBrowser", show, window_flags);
	// Don't want to accidentally render something totally arbitrary in memory
	if (!browser_tab_visible) {
		render_data.changed = false;
	}
	ImGuiWindowFlags config_window_flags = 0;

	// Config window
	if (show_config) {
		ImGui::SetNextWindowSize(ImVec2(300, 200), ImGuiCond_Always);
		ImGui::Begin("EQ Browser Config", &show_config, config_window_flags);
		ImGui::Checkbox("Background Visible", &background);
		ImGui::SliderFloat("Opacity", &opacity, 0.0f, 1.0f);
		if (ImGui::SliderFloat("Volume", &volume, 0.0f, 1.0f)) {
			SetAudioVolume();
		}

		if (ImGui::SliderFloat("Zoom", &zoom, -2.0f, 2.0f)) {
			SetZoomLevel();
		}
		// This causes problems. Don't always allow dev tools.
#ifdef MQBROWSER_DEV
		if (ImGui::Button("Show Dev Tools"))
		{
			CefWindowInfo windowInfo;
			windowInfo.SetAsPopup(NULL, "DevTools");
			web_browser->GetHost()->ShowDevTools(windowInfo, this, CefBrowserSettings(), CefPoint());
			browser_has_focus = false;
		}
#endif
		ImGui::End();

	}

	//Menu Bar
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(5, 5));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize, ImVec2(20, 150));
	if (ImGui::BeginMenuBar())
	{
		if (ImGui::BeginMenu("Menu"))
		{
			if (ImGui::MenuItem(show_config ? ICON_MD_SETTINGS " Hide Config" : ICON_MD_SETTINGS " Show Config"))
			{
				show_config = !show_config;
			}
			if (ImGui::MenuItem(ICON_MD_DASHBOARD " Go to Dashboard"))
			{
				loadNewUrl = true;
				skip_validation = true;
				strcpy(url_buffer, "https://mq2dashboard.vercel.app/");
			}
			ImGui::EndMenu();
		}
		if (!can_back) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		if (ImGui::Button(ICON_MD_ARROW_BACK))
		{
			const std::string goBack("window.history.go(-1);");
			web_browser->GetMainFrame()->ExecuteJavaScript(goBack, "", 0);
		}
		if (!can_back) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}

		// Forward button
		ImGui::SameLine();
		if (!can_forward) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		if (ImGui::Button(ICON_MD_ARROW_FORWARD))
		{
			const std::string goForward("window.history.go(1);");
			web_browser->GetMainFrame()->ExecuteJavaScript(goForward, "", 0);
		}
		if (!can_forward) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}

		// Home Button
		ImGui::SameLine();
		if (ImGui::Button(ICON_MD_HOME))
		{
			loadNewUrl = true;
			strcpy(url_buffer, "https://www.google.com/");
		}

		// URL bar
		ImGui::SameLine();
		if (ImGui::InputText("URL", url_buffer, 512, ImGuiInputTextFlags_EnterReturnsTrue)) {
			loadNewUrl = true;
		}

		// If we focus for the first time on the input bar
		input_focused = ImGui::IsItemActive();
		if (input_focused && !input_has_focus) {
			const std::string goBack("document.activeElement && document.activeElement.blur();");
			if (web_browser->GetMainFrame()) {
				web_browser->GetMainFrame()->ExecuteJavaScript(goBack, "", 0);
			}
			input_has_focus = true;
		}
		else if (!input_focused) { input_has_focus = false; }

		ImGui::SameLine();
		if (ImGui::Button("Go"))
		{
			loadNewUrl = true;
			skip_validation = true;
		}
		ImGui::EndMenuBar();
		
	}
	ImGui::PopStyleVar();
	ImGui::PopStyleVar();

	if (loadNewUrl) {
		// do some scrubbing to create a fully qualified url.. this is pretty basic
		// if https:// doesn't exist, prepend
		// url validation is tricky. for now just make go button go exactly to url
		// and enter do some work
		std::string load_url = std::string(url_buffer);
		if (!skip_validation) {
			// Let's see if they are trying to do a google search
			std::regex url_regex("http|(?:^\w+\.\w+$)|localhost");
			if (!std::regex_match(load_url, url_regex)) {
				std::replace(load_url.begin(), load_url.end(), ' ', '+');
				load_url = "https://www.google.com/search?q=" + load_url;
			}
			else {
				int size = url.size();
				if (!(load_url == "http://localhost:3000" || load_url.find("mq://") != std::string::npos)) {
					if (load_url.rfind("https://", 0) != 0) {
						load_url = "https://" + load_url;
					}

					// if https://www doesn't exist, replace
					if (load_url.rfind("https://www.", 0) != 0) {
						load_url = load_url.replace(load_url.find("https://"), 8, "https://www.");
					}
					// add trailing slash for qualified url
					if (load_url.find_last_of('/', 0) != load_url.size() - 1) {
						load_url = load_url.append("/");
					}
				}
			}

		}

		web_browser->GetMainFrame()->LoadURL(load_url);
	}

	// If we're not in a docked display and hidden, render the browser
	if (browser_tab_visible) {

		// Now start rendering from CEF
		ImVec2 avail_size = ImGui::GetContentRegionAvail();

		if (avail_size.x == 0 || avail_size.y == 0) {
			ImGui::End();
			return;
		}

		// Handle resize
		if ((int)avail_size.x != width || (int)avail_size.y != height) {
			width = avail_size.x;
			height = avail_size.y;

			if (pTexture) {
				(pTexture)->Release();
				(pTexture) = NULL;
			}
#ifdef MQ_BROWSER
			if (pGraphicsEngine && pGraphicsEngine->pRender && pGraphicsEngine->pRender->pD3DDevice)
			{
				device = pGraphicsEngine->pRender->pD3DDevice;
			}
			else {
				WriteChatColor("Could not find display device to render browser");
				ImGui::End();
				ImGui::PopStyleColor();
				ImGui::PopStyleVar();
				return;
			}
#endif
			auto result =
				D3DXCreateTexture(device, width, height, 1, 0, D3DFMT_A8R8G8B8,
					D3DPOOL_MANAGED, &pTexture);
			// Send resize event to CEF
			if (web_browser)
				web_browser->GetHost()->WasResized();


			// Tell CEF to render a new frame
			render_data.cv.notify_all();
			ImGui::End();
			ImGui::PopStyleColor();
			ImGui::PopStyleVar();
			return;
		}

		IDirect3DSurface9* pSurface;

		pTexture->GetSurfaceLevel(0, &pSurface);

		if (render_data.changed && render_data.width == width &&
			render_data.height == height) {
			// Lock surface
			D3DLOCKED_RECT LockedRect;
			if (FAILED(pSurface->LockRect(&LockedRect, nullptr, 0))) {
				render_data.cv.notify_all();
				return;
			}

			// Update changed state
			render_data.changed = false;

			D3DSURFACE_DESC SurfaceDesc;
			IDirect3DSurface9* pSurface;

			pTexture->GetSurfaceLevel(0, &pSurface);

			pSurface->GetDesc(&SurfaceDesc);
			pSurface->LockRect(&LockedRect, nullptr, 0);

			auto surfaceData = static_cast<byte*>(LockedRect.pBits);
			auto sourceData = static_cast<const byte*>(render_data.buffer);
			auto pitch = LockedRect.Pitch;
			if (render_data.dirtyRects.size() > 0 &&
				render_data.dirtyRects[0].width == render_data.width &&
				render_data.dirtyRects[0].height == render_data.height) {
				// Update whole texture
				memcpy(surfaceData, sourceData,
					render_data.width * render_data.height * 4);
			}
			else {
				// Update dirty rects
				for (auto& rect : render_data.dirtyRects) {
					for (int y = rect.y; y < rect.y + rect.height; ++y) {
						int index = y * pitch + rect.x * 4;
						memcpy(&surfaceData[index], &sourceData[index], rect.width * 4);
					}
				}
			}

			// Unlock surface
			pSurface->UnlockRect();
			render_data.cv.notify_all();
		}

		// Render image from CEF
		ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * opacity);
		ImGui::Image((void*)pTexture, ImVec2(render_data.width, render_data.height));
		ImGui::PopStyleVar();


		// If we're focused, capture keyboard events
		if (browser_has_focus && !input_focused) {
			ImGuiStyle& style = ImGui::GetStyle();
			style.WindowBorderSize = 2.0f;
			ImGui::CaptureKeyboardFromApp(true);
			for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++)
				if (ImGui::IsKeyPressed(i)) {
					CefKeyEvent keyEvent;
					keyEvent.is_system_key = false;
					keyEvent.windows_key_code = i;
					int mods = EVENTFLAG_NONE;
					if (io.KeyCtrl) {
						mods |= EVENTFLAG_CONTROL_DOWN;
					}
					if (io.KeyShift) {
						mods |= EVENTFLAG_SHIFT_DOWN;
					}
					if (io.KeyAlt) {
						mods |= EVENTFLAG_ALT_DOWN;
					}

					keyEvent.modifiers = mods;
					keyEvent.type = KEYEVENT_RAWKEYDOWN;
					web_browser->GetHost()->SendKeyEvent(keyEvent);

					keyEvent.type = KEYEVENT_KEYUP;
					web_browser->GetHost()->SendKeyEvent(keyEvent);
				}

			// Keyboard char input
			for (int i = 0; i < io.InputQueueCharacters.Size; i++) {
				CefKeyEvent keyEvent;
				keyEvent.windows_key_code = io.InputQueueCharacters[i];
				keyEvent.character = io.InputQueueCharacters[i];
				keyEvent.is_system_key = false;
				int mods = EVENTFLAG_NONE;
				if (io.KeyCtrl) {
					mods |= EVENTFLAG_CONTROL_DOWN;
				}
				if (io.KeyShift) {
					mods |= EVENTFLAG_SHIFT_DOWN;
				}
				if (io.KeyAlt) {
					mods |= EVENTFLAG_ALT_DOWN;
				}

				keyEvent.modifiers = mods;

				keyEvent.type = KEYEVENT_CHAR;
				web_browser->GetHost()->SendKeyEvent(keyEvent);
			}
		}
		else {
			ImGuiStyle& style = ImGui::GetStyle();
			style.WindowBorderSize = 0.0f;
		}

		// If we're hovered, handle mouse events
		browser_hovered = ImGui::IsItemHovered();
		if (browser_hovered) {
			ImVec2 mousePositionAbsolute = ImGui::GetMousePos();
			ImVec2 screenPositionAbsolute = ImGui::GetItemRectMin();
			ImVec2 mousePositionRelative =
				ImVec2(mousePositionAbsolute.x - screenPositionAbsolute.x,
					mousePositionAbsolute.y - screenPositionAbsolute.y);
			CefMouseEvent mouseEvent;
			mouseEvent.x = mousePositionRelative.x;
			mouseEvent.y = mousePositionRelative.y;
			if (ImGui::GetIO().MouseWheel != 0) {
				web_browser->GetHost()->SendMouseWheelEvent(mouseEvent, 0,
					io.MouseWheel * 20);
			}

			// Left click up
			if (!ImGui::IsMouseDown(ImGuiMouseButton_Left) &&
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_LEFT)]) {
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_LEFT)] = false;
				web_browser->GetHost()->SendMouseClickEvent(
					mouseEvent,
					static_cast<CefBrowserHost::MouseButtonType>(
						eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_LEFT),
					true, 1);
			}

			// right click up
			if (!ImGui::IsMouseDown(ImGuiMouseButton_Right) &&
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_RIGHT)]) {
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_RIGHT)] = false;
				web_browser->GetHost()->SendMouseClickEvent(
					mouseEvent,
					static_cast<CefBrowserHost::MouseButtonType>(
						eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_RIGHT),
					true, 1);
			}
			// middle click up
			if (!ImGui::IsMouseDown(ImGuiMouseButton_Middle) &&
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_MIDDLE)]) {
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_MIDDLE)] = false;
				web_browser->GetHost()->SendMouseClickEvent(
					mouseEvent,
					static_cast<CefBrowserHost::MouseButtonType>(
						eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_MIDDLE),
					true, 1);
			}

			// New left click down
			if (ImGui::IsMouseDown(ImGuiMouseButton_Left) &&
				!m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_LEFT)]) {
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_LEFT)] = true;
				web_browser->GetHost()->SetFocus(true);
				browser_has_focus = true;
				web_browser->GetHost()->SendMouseClickEvent(
					mouseEvent,
					static_cast<CefBrowserHost::MouseButtonType>(
						eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_LEFT),
					false, 1);
			}
			// New right click down
			if (ImGui::IsMouseDown(ImGuiMouseButton_Right) &&
				!m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_RIGHT)]) {
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_RIGHT)] = true;
				web_browser->GetHost()->SetFocus(true);
				browser_has_focus = true;
				web_browser->GetHost()->SendMouseClickEvent(
					mouseEvent,
					static_cast<CefBrowserHost::MouseButtonType>(
						eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_RIGHT),
					false, 1);
			}
			// New middle click down
			if (ImGui::IsMouseDown(ImGuiMouseButton_Middle) &&
				!m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_MIDDLE)]) {
				m_mouseButtonStates[static_cast<int>(
					eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_MIDDLE)] = true;
				web_browser->GetHost()->SetFocus(true);
				browser_has_focus = true;
				web_browser->GetHost()->SendMouseClickEvent(
					mouseEvent,
					static_cast<CefBrowserHost::MouseButtonType>(
						eWebBrowserMouseButton::BROWSER_MOUSEBUTTON_MIDDLE),
					false, 1);
			}
			else {
				if (m_mouseButtonStates[BROWSER_MOUSEBUTTON_LEFT])
					mouseEvent.modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
				if (m_mouseButtonStates[BROWSER_MOUSEBUTTON_MIDDLE])
					mouseEvent.modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
				if (m_mouseButtonStates[BROWSER_MOUSEBUTTON_RIGHT])
					mouseEvent.modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;
				web_browser->GetHost()->SendMouseMoveEvent(mouseEvent, false);
			}
		}
		else if (!ImGui::IsWindowFocused()) {
			const std::string goBack("document.activeElement && document.activeElement.blur();");
			if (web_browser->GetMainFrame()) {
				web_browser->GetMainFrame()->ExecuteJavaScript(goBack, "", 0);
				browser_has_focus = false;
			}
		}
	}

	ImGui::End();
	
	ImGui::PopStyleVar();
	ImGui::PopStyleColor();

	
	
	render_data.cv.notify_all();
}

// Not in use yet
void BrowserClient::Shutdown() {
	render_data.cv.notify_all();
	if (web_browser->GetHost()->TryCloseBrowser()) {
		CefShutdown();
	}
}

void BrowserClient::Pause() {
	render_data.cv.notify_all();
	web_browser->GetHost()->WasHidden(true);
}
void BrowserClient::Resume() {
	render_data.cv.notify_all();
	web_browser->GetHost()->WasHidden(false);
}
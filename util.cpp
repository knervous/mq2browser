#include "util.h"

bool is_number(const std::string& s)
{
	std::string::const_iterator it = s.begin();
	while (it != s.end() && std::isdigit(*it)) ++it;
	return !s.empty() && it == s.end();
}

bool parse_double(const std::string in, double& res) {
	try {
		size_t read = 0;
		res = std::stod(in, &read);
		if (in.size() != read)
			return false;
	}
	catch (std::invalid_argument) {
		return false;
	}
	return true;
}